import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('Sign In Page/Textfield - Username'), username)

WebUI.setText(findTestObject('Sign In Page/Textfield - Password'), password)

WebUI.click(findTestObject('Sign In Page/Btn - Login'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Landing Page/Title - Core'), 0)

WebUI.verifyElementPresent(findTestObject('Landing Page/Title - Core'), 0)

WebUI.verifyElementPresent(findTestObject('Landing Page/Btn - Bank'), 0)

WebUI.verifyElementPresent(findTestObject('Landing Page/Btn - Nasabah'), 0)

WebUI.verifyElementPresent(findTestObject('Landing Page/Btn - Pinjaman'), 0)

WebUI.verifyElementPresent(findTestObject('Landing Page/Btn - Layanan'), 0)

WebUI.verifyElementPresent(findTestObject('Landing Page/Btn - Product'), 0)

WebUI.verifyElementPresent(findTestObject('Landing Page/Btn - Tipe Bank'), 0)

WebUI.verifyElementPresent(findTestObject('Landing Page/Btn - Tujuan'), 0)

WebUI.verifyElementPresent(findTestObject('Landing Page/Btn - Role'), 0)

WebUI.verifyElementPresent(findTestObject('Landing Page/Btn - Logout'), 0)

