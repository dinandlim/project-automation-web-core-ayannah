import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
//import com.mysql.jdbc.Connection
import java.sql.DriverManager as DriverManager
//import com.mysql.jdbc.Connection
import java.sql.PreparedStatement as PreparedStatement
import java.sql.Connection as Connection
import java.sql.Statement as Statement
import java.sql.SQLException as SQLException

String SALTCHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

StringBuilder salt = new StringBuilder()

Random rnd = new Random()

while (salt.length() < 10) {
    // length of the random string.
    int index = ((rnd.nextFloat() * SALTCHARS.length()) as int)

    salt.append(SALTCHARS.charAt(index))
}

String ServiceName = 'Pinjaman ' + salt.toString()

WebUI.setText(findTestObject('Service Page/Service Add/Textfield - Service Name'), ServiceName)

WebUI.sendKeys(findTestObject('Service Page/Service Add/Btn - Browse Image'), 'C:\\Users\\inotech\\Desktop\\network.png')

WebUI.click(findTestObject('Service Page/Service Add/Checkbox - Status'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Service Page/Service Add/Btn - Batal'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Service Page/Service List/Title - Layanan List'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Title - Layanan List'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Autonumbering'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Id'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Name'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Status'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Action'), 0)

WebUI.callTestCase(findTestCase('Service/TC-03'), [:], FailureHandling.STOP_ON_FAILURE)

