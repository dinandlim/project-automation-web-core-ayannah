import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.DriverManager as DriverManager
import java.sql.PreparedStatement as PreparedStatement
import java.sql.Connection as Connection
import java.sql.Statement as Statement
import java.sql.SQLException as SQLException

WebUI.sendKeys(findTestObject('Service Page/Service Add/Btn - Browse Image'), 'C:\\Users\\inotech\\Desktop\\network.png')

WebUI.click(findTestObject('Service Page/Service Add/Checkbox - Status'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Service Page/Service Edit/Btn - Ubah'))

WebUI.waitForElementVisible(findTestObject('Service Page/Service Edit/Popup Text - Berhasil Edit'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service Edit/Popup Text - Berhasil Edit'), 0)

WebUI.click(findTestObject('Service Page/Service Edit/Popup Button - Ok'), FailureHandling.STOP_ON_FAILURE)

DBData data = findTestData('Service Page')

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    nama_layanan = data.getValue(1, index)

    status_layanan = data.getValue(2, index)

    WebUI.waitForElementVisible(findTestObject('Service Page/Service List/Title - Layanan List'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Title - Layanan List'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Autonumbering'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Id'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Name'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Status'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Action'), 0)

    lbl_Status = new TestObject('')

    lbl_Status.addProperty('xpath', ConditionType.EQUALS, ('//h2[text()="Layanan List"]/../table/tbody/tr/td[contains(.,"' + 
        nama_layanan) + '")]/following-sibling::td[1]')

    CurrentStatus = WebUI.getText(lbl_Status)

    Mobile.verifyMatch(CurrentStatus, 'Aktif', false)
	
	
	//insert status to database
	String driver = 'com.postgresql.jdbc.driver'
	
	String url = 'jdbc:postgresql://35.240.150.90:5432/db_qa'
	
	String uname = 'asirauser'
	
	String pass = 'asirapass'
	
	Connection con = DriverManager.getConnection(url, uname, pass)
	
	PreparedStatement ps = con.prepareStatement('UPDATE core SET status = (?) ')
	
	ps.setString(1, CurrentStatus)
	
	int j = ps.executeUpdate()
	
	if (j == 1) {
		System.out.println('data inserted')
	} else {
		System.out.println('not inserted')
	}
}

