import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('Service Page')

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    nama_layanan = data.getValue(1, index)

    status_layanan = data.getValue(2, index)

    lbl_Edit = new TestObject('')

    lbl_Edit.addProperty('xpath', ConditionType.EQUALS, ('//h2[text()="Layanan List"]/../table/tbody/tr/td[contains(.,"' + 
        nama_layanan) + '")]/following-sibling::td/a/i[@class="fas fa-edit"]')

    WebUI.click(lbl_Edit)

    WebUI.waitForElementVisible(findTestObject('Service Page/Service Edit/Title - Service Update'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service Edit/Title - Service Update'), 0)

    lbl_Nama_Layanan = new TestObject('')

    lbl_Nama_Layanan.addProperty('xpath', ConditionType.EQUALS, ('//label[text()="Nama Layanan"]/../div/input[@placeholder="' + 
        nama_layanan) + '"]')

    WebUI.verifyElementPresent(lbl_Nama_Layanan, 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service Edit/Btn - Edit Picture'), 0)

    //get status sekarang
    CurrentStatus = WebUI.getText(findTestObject('Service Page/Service Edit/Text - Current Status'))

    WebUI.verifyMatch(status_layanan, CurrentStatus, false)
    
    WebUI.verifyElementPresent(findTestObject('Service Page/Service Edit/Btn - Ubah'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service Edit/Btn - Batal'), 0)
}

