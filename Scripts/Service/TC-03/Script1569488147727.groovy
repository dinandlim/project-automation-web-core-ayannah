import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Landing Page/Btn - Layanan'))

WebUI.waitForElementVisible(findTestObject('Service Page/Btn - Tambah'), 0)

WebUI.click(findTestObject('Service Page/Btn - Tambah'))

WebUI.waitForElementVisible(findTestObject('Service Page/Service Add/Title - Service Add'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service Add/Title - Service Add'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service Add/Textfield - Service Name'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service Add/Btn - Browse Image'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service Add/Checkbox - Status'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service Add/Btn - Simpan'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service Add/Btn - Batal'), 0)

