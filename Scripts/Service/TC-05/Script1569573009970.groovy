import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.DriverManager as DriverManager
import java.sql.PreparedStatement as PreparedStatement
import java.sql.Connection as Connection
import java.sql.Statement as Statement
import java.sql.SQLException as SQLException

//random nama Layanan
String SALTCHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

StringBuilder salt = new StringBuilder()

Random rnd = new Random()

while (salt.length() < 10) {
    // length of the random string.
    int index = ((rnd.nextFloat() * SALTCHARS.length()) as int)

    salt.append(SALTCHARS.charAt(index))
}

String ServiceName = 'Pinjaman ' + salt.toString()

//continue testcase
WebUI.setText(findTestObject('Service Page/Service Add/Textfield - Service Name'), ServiceName)

WebUI.sendKeys(findTestObject('Service Page/Service Add/Btn - Browse Image'), 'C:\\Users\\inotech\\Desktop\\network.png')

WebUI.click(findTestObject('Service Page/Service Add/Checkbox - Status'), FailureHandling.STOP_ON_FAILURE)

ServiceStatus = WebUI.getText(findTestObject('Service Page/Service Add/Text - Status'))

WebUI.click(findTestObject('Service Page/Service Add/Btn - Simpan'), FailureHandling.STOP_ON_FAILURE)

//insert nama layanan ke database
String driver = 'com.postgresql.jdbc.driver'

String url = 'jdbc:postgresql://35.240.150.90:5432/db_qa'

String uname = 'asirauser'

String pass = 'asirapass'

Connection con = DriverManager.getConnection(url, uname, pass)

PreparedStatement ps = con.prepareStatement('UPDATE core SET service_name = (?) , status = (?)')

ps.setString(1, ServiceName)

ps.setString(2, ServiceStatus)

int j = ps.executeUpdate()

if (j == 1) {
    System.out.println('data inserted')
} else {
    System.out.println('not inserted')
}

//Handling pop up success
WebUI.waitForElementVisible(findTestObject('Service Page/Service Add/Text - Popup Success Add'), 0)

WebUI.verifyElementPresent(findTestObject('Service Page/Service Add/Text - Popup Success Add'), 0)

WebUI.click(findTestObject('Service Page/Service Add/Btn - Popup Success Add'), FailureHandling.STOP_ON_FAILURE)

//Using Database
DBData data = findTestData('Service Page')

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    nama_layanan = data.getValue(1, index)

    status_layanan = data.getValue(2, index)

    //verify layanan list dan list, status yang telah dibuat
    WebUI.waitForElementVisible(findTestObject('Service Page/Service List/Title - Layanan List'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Title - Layanan List'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Autonumbering'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Id'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Name'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Status'), 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service List/Subtitle - Service Action'), 0)

    lbl_Nama_Layanan = new TestObject('')

    lbl_Nama_Layanan.addProperty('xpath', ConditionType.EQUALS, ('//h2[text()="Layanan List"]/../table/tbody/tr/td[contains(.,"' + 
        nama_layanan) + '")]/following-sibling::td[1]')

    lbl_status_layanan = WebUI.getText(lbl_Nama_Layanan)

    WebUI.verifyMatch(status_layanan, lbl_status_layanan, false) 
	
	//Insert Status to Database
    // String driver2 = 'com.postgresql.jdbc.driver'
    //String url2 = 'jdbc:postgresql://35.240.150.90:5432/db_qa'
    //String uname2 = 'asirauser'
    //String pass2 = 'asirapass'
    //Connection con2 = DriverManager.getConnection(url2, uname2, pass2)
    //PreparedStatement ps2 = con2.prepareStatement('UPDATE core SET status = (?) ')
    //ps2.setString(1, Status)
    //int k = ps2.executeUpdate()
    //if (k == 1) {
    //  System.out.println('data inserted')
    //} else {
    //   System.out.println('not inserted')
    //}
}

WebUI.callTestCase(findTestCase('Service/TC-03'), [:], FailureHandling.STOP_ON_FAILURE)

