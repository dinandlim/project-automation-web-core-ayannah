import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('Service Page')

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    nama_layanan = data.getValue(1, index)

    status_layanan = data.getValue(2, index)

    btn_view = new TestObject('')

    btn_view.addProperty('xpath', ConditionType.EQUALS, ('//h2[text()="Layanan List"]/../table/tbody/tr/td[contains(.,"' + 
        nama_layanan) + '")]/following-sibling::td/a/i[@class="fas fa-eye"]')

    WebUI.click(btn_view)

    WebUI.waitForElementVisible(findTestObject('Service Page/Service Detail/Text - Service Name'), 0)

    lbl_Nama_Layanan = new TestObject('')

    lbl_Nama_Layanan.addProperty('xpath', ConditionType.EQUALS, ('//label[text()="Nama Layanan"]/../div[contains(.,"' + 
        nama_layanan) + '")]')

    lbl_Status = new TestObject('')

    lbl_Status.addProperty('xpath', ConditionType.EQUALS, ('//label[text()="Status"]/../div[contains(.,"' + status_layanan) + '")]')

    WebUI.verifyElementPresent(findTestObject('Service Page/Service Detail/Title - Service Detail'), 0)

    WebUI.verifyElementPresent(lbl_Nama_Layanan, 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service Detail/Text - Service Picture'), 0)

    WebUI.verifyElementPresent(lbl_Status, 0)

    WebUI.verifyElementPresent(findTestObject('Service Page/Service Detail/Btn - Back'), 0)
}

