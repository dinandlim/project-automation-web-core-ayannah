import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('Service Page')

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    nama_layanan = data.getValue(1, index)

    status_layanan = data.getValue(2, index)

    lbl_Detail = new TestObject('')

    lbl_Detail.addProperty('xpath', ConditionType.EQUALS, ('//h2[text()="Layanan List"]/../table/tbody/tr/td[contains(.,"' + 
        nama_layanan) + '")]/following-sibling::td/a/i[@class="fas fa-eye"]')

    WebUI.click(lbl_Detail)

    lbl_nama_layanan = new TestObject('')

    detail_nama_layanan = lbl_nama_layanan.addProperty('xpath', ConditionType.EQUALS, ('//label[text()="Nama Layanan"]/../div[contain(.,"' + 
        nama_layanan) + '")]')

    WebUI.getText(detail_nama_layanan)

    lbl_status_layanan = new TestObject('')

    detail_status_layanan = lbl_status_layanan.addProperty('xpath', ConditionType.EQUALS, ('//label[text()="Status"]/../div[contain(.,"' + 
        status_layanan) + '")]')

    WebUI.getText(detail_status_layanan)

    WebUI.verifyMatch(nama_layanan, , false)

    WebUI.verifyMatch(status_layanan, '', false)
}

