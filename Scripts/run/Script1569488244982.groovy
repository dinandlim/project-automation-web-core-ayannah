import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.callTestCase(findTestCase('Sign In/TC-01'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Sign In/TC-02'), [('username') : 'adminkey', ('password') : 'adminsecret'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Service/TC-03'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Service/TC-04'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Service/TC-05'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Service/TC-06'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Service/TC-07'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Service/TC-08'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Service/TC-09'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Service/TC-10'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Service/TC-11'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Service/TC-12'), [:], FailureHandling.STOP_ON_FAILURE)

