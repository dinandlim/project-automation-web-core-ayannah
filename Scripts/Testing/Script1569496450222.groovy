import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.DriverManager
//import com.mysql.jdbc.Connection
import java.sql.PreparedStatement
import java.sql.Connection
import java.sql.Statement;
import java.sql.SQLException;

String SALTCHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

StringBuilder salt = new StringBuilder()

Random rnd = new Random()

while (salt.length() < 10) {
	// length of the random string.
	int index = ((rnd.nextFloat() * SALTCHARS.length()) as int)

	salt.append(SALTCHARS.charAt(index))
}

String ServiceName = "Pinjaman " + salt.toString()

//WebUI.setText(findTestObject('Service Page/Service Add/Textfield - Service Name'), ServiceName)

String driver="com.postgresql.jdbc.driver";
String url="jdbc:postgresql://35.240.150.90:5432/db_qa";
String uname="asirauser";
String pass="asirapass";
//Class.forName(driver);
Connection con = DriverManager.getConnection(url,uname,pass);
PreparedStatement ps=con.prepareStatement("UPDATE core SET service_name = (?) ");
ps.setString(1, ServiceName);
   int j =ps.executeUpdate();
   if(j==1)
   {
   System.out.println("data inserted");
   }
   else
   {
   System.out.println("not inserted");
   }