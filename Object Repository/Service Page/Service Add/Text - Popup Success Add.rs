<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text - Popup Success Add</name>
   <tag></tag>
   <elementGuidId>c4dde836-4666-4219-991a-bd647179bc2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@role=&quot;dialog&quot;]/div[contains(.,&quot;Success&quot;)]/following-sibling::div[contains(.,&quot;Layanan berhasil di tambah&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
