<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Subtitle - Service Status</name>
   <tag></tag>
   <elementGuidId>346caf3b-4f5c-4b08-93ba-3994f5aa8da2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h2[text()=&quot;Layanan List&quot;]/../table/thead/tr/th[contains(.,&quot;Status Layanan&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
